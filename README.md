**Report is on the bottom!!**
---------------------------

# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---------------------------------------------------

# Report

### Website link: [105062315 Canvas][canvas]

### Basic component
 1. basic drawing tool:
   * brush
   * eraser
   * text input
 2. simple menu:
   * brush size
   * color selector
   * font size
   * different font family
 3. different cursor: 
    ![arror](/reportImg/arrow.png) ![circle](/reportImg/circle.png) ![triangle](/reportImg/triangle.png) ![rectangle](/reportImg/rectangle.png) ![line](/reportImg/line.png) ![eraser](/reportImg/eraser.png) ![stamp](/reportImg/stamp.png)
 4. refresh button
 
### Advance function
 1. advance drawing tool:
   * circle
   * rectangle
   * triangle
 2. un/redo function (max support to 50 steps undo)
 3. stamp tool (used to insert image)
 4. download function

### Bonus function
 1. line drawing function:
 2. fill/stroke control: (only for circle/rectangle/triangle and font)
  * fill the shape when drawing   
    ![fill style](/reportImg/fill.png)
  * stroke the shape when drawing   
    ![fill style](/reportImg/stroke.png)
 3. un/redo status reminder:
  * if un/redo is not available, it'll change to gray color
  * ![undo unavailable](/reportImg/undo_unavail.png)
  * ![redo unavailable](/reportImg/redo_unavail.png)
 4. selectable stamp size:
  * user can resize their image when pasting
  * ![customize stamp size](/reportImg/stampSize.png)
 5. hide/show corresponded menu for different brush:
  * brush menu   
    ![brush menu](/reportImg/brushMenu.png)
  * text menu   
    ![text menu](/reportImg/textMenu.png)
  * stamp menu   
    ![stamp menu](/reportImg/stampMenu.png)
 6. Selectable canvas background color   
    ![canvas menu](/reportImg/canvasBackground.png)

[canvas]: https://105062315.gitlab.io/AS_01_WebCanvas/