
var canvas = document.querySelector("canvas");
var ctx = canvas.getContext("2d");
var type = 0;	// 0=brush; 1=circle; 2=rect; 3=tri, 4=line; 5=eraser; 6=text; 7=img;
var textSetting = document.querySelector(".textSetting");
var textContent = document.querySelector("#textContent");
var imageSetting = document.querySelector(".imgSetting");
var getImage = document.querySelector("#imgUpload");
var img = {
	url: "empty",
	width: document.querySelector("#imgWidth"),
	height: document.querySelector("#imgHeight")
}
var fontSelector = document.querySelector("#font");
var fontSize = document.querySelector("#fontSize");
var showFontSize = document.querySelector("#showSize");
var borderWidth = document.querySelector("#borderWidth");
var showBorderWidth = document.querySelector("#showWidth");
var canvasColorSelector = document.querySelector("#canvasColor");
var colorSelector = document.querySelector("#brushColor");
var fillStyle = document.querySelector("#fillStyle");
var btn = {
	brush: document.querySelector("#brush"),
	circle: document.querySelector("#circle"),
	rect: document.querySelector("#rect"),
	tri: document.querySelector("#triangle"),
	line: document.querySelector("#line"),
	eraser: document.querySelector("#eraser"),
	text_: document.querySelector("#text"),
	img: document.querySelector("#img")
};
var func = {
	undo: document.querySelector("#undo"),
	rst: document.querySelector("#reset"),
	redo: document.querySelector("#redo"),
	download: document.querySelector("#download")	
}
var startX, startY;		// record mouse index when mouse down
var stepRecord = [];	// record screen shot after every step, max=50
var stepIndex;			// record current index of stepRecord
var maxRedoStep;		// record max stepIndex for redo

// initialize
window.onload = function(){
	windowReset();
};

function windowReset(){
	// initialize to brush
	type = 0;
	canvasColorSelector.value = "#FFFFFF";
	colorSelector.value = "#000000";
	fillStyle.value = "fill";
	fontSize.value = 12;
	borderWidth.value = 1;
	getImage.value = "";
	ctx.fillStyle = "#000000";
	ctx.strokeStyle = "#000000";
	ctx.lineWidth = 1;
	stepIndex = -1;
	maxRedoStep = -1;
	
	// clean canvas
	ctx.clearRect(0, 0, canvas.width, canvas.height);
}

// record max to 50 steps user make
function recordStep(){
	stepRecord.push(canvas.toDataURL());
	if(stepIndex==49){
		stepRecord.shift();
	}else{
		++stepIndex;
		maxRedoStep = stepIndex;
	}
	func.undo.style.backgroundImage = "url('img/button/undo.png')";
	func.redo.style.backgroundImage = "url('img/button/redo_ban.png')";
}

function getRelativePos(e){
	var canvasBox = canvas.getBoundingClientRect();
	var pos = {
		x: e.clientX - canvasBox.left,
		y: e.clientY - canvasBox.top
	};
	return pos;
}

// mouse move
function mouseMove(e){
	var mousePos = getRelativePos(e);
	if(type==0 || type==5){
		// brush or eraser
		ctx.lineTo(mousePos.x, mousePos.y);
		ctx.stroke();
	}
}

// Mouse down
canvas.addEventListener("mousedown", function(e){
	// get mouse position relative to canvas
	var mousePos = getRelativePos(e);
	startX = mousePos.x;
	startY = mousePos.y;
	
	ctx.beginPath();
	if(type==0 || type==5){
		// brush and eraser
		ctx.moveTo(startX, startY);
		canvas.addEventListener("mousemove", mouseMove);
	}
})

// Mouse up
canvas.addEventListener("mouseup", function(e){
	var mousePos = getRelativePos(e);
	if(type==0 || type==5){
		// brush or eraser
		canvas.removeEventListener("mousemove", mouseMove);
	}else if(type==1){
		// circle
		var radius = Math.max(Math.abs(startX-mousePos.x), Math.abs(startY-mousePos.y)) / 2;
		ctx.arc(Math.min(startX, mousePos.x)+radius, Math.min(startY, mousePos.y)+radius, radius, 0, 2*Math.PI);
		if(fillStyle.value=="fill")
			ctx.fill();
		else
			ctx.stroke();
	}else if(type==2){
		// rectangle
		ctx.rect(Math.min(startX, mousePos.x), Math.min(startY, mousePos.y), Math.abs(startX-mousePos.x), Math.abs(startY-mousePos.y));
		if(fillStyle.value=="fill")
			ctx.fill();
		else
			ctx.stroke();
	}else if(type==3){
		// triangle
		ctx.moveTo(startX, mousePos.y);
		ctx.lineTo(mousePos.x, mousePos.y);
		ctx.lineTo((startX+mousePos.x)/2, startY);
		if(fillStyle.value=="fill")
			ctx.fill();
		else{
			ctx.closePath();
			ctx.stroke();
		}
	}else if(type==4){
		// line
		ctx.moveTo(startX, startY);
		ctx.lineTo(mousePos.x, mousePos.y);
		ctx.stroke();
	}else if(type==6){
		// text
		ctx.font = fontSize.value + "px " + fontSelector.value;
		if(fillStyle.value=="fill")
			ctx.fillText(textContent.value, mousePos.x, mousePos.y);
		else
			ctx.strokeText(textContent.value, mousePos.x, mousePos.y);
	}else if(type==7){
		// image
		var htmlImg = new Image();
		htmlImg.onload = function(){
			ctx.drawImage(htmlImg, mousePos.x-(img.width.value/2), mousePos.y-(img.height.value/2), img.width.value, img.height.value);
		};
		htmlImg.src = img.url;
	}
	recordStep();
});

// mouse out
canvas.addEventListener("mouseleave", function(){
	// stop drawing
	canvas.removeEventListener("mousemove", mouseMove);
});

// mouse in
canvas.addEventListener("mouseenter", function(){
	// change cursor
	// 0=brush; 1=circle; 2=rect; 3=tri, 4=line; 5=eraser; 6=text
	switch(type){
	case 1:
		canvas.style.cursor = "url('img/cursor/circle.cur'), auto";
		break;
	case 2:
		canvas.style.cursor = "url('img/cursor/rectangle.cur'), auto";
		break;
	case 3:
		canvas.style.cursor = "url('img/cursor/triangle.cur'), auto";
		break;
	case 4:
		canvas.style.cursor = "url('img/cursor/line.cur'), auto";
		break;
	case 5:
		canvas.style.cursor = "url('img/cursor/eraser.cur'), auto";
		break;
	case 6:
		canvas.style.cursor = "text";
		break;
	case 7:
		canvas.style.cursor = "url('img/cursor/stamp.cur'), auto";
		break;
	default:
		canvas.style.cursor = "url('img/cursor/arrow.cur'), auto";
		break;
	}
});

/* Selector */
// user change brush color
colorSelector.addEventListener("change", function(){
	ctx.strokeStyle = colorSelector.value;
	ctx.fillStyle = colorSelector.value;
});

// user change canvas color
canvasColorSelector.addEventListener("input", function(){
	canvas.style.backgroundColor = canvasColorSelector.value;
});

// when user change size
borderWidth.addEventListener("input", function(){
	showBorderWidth.textContent = borderWidth.value;
});
borderWidth.addEventListener("change", function(){
	ctx.lineWidth = borderWidth.value;
});
fontSize.addEventListener("input", function(){
	showFontSize.textContent = fontSize.value;
});

// upload image
getImage.addEventListener("change", function(){
	var fileList = this.files;
	var reader = new FileReader();
	reader.onload = function (e){
		img.url = e.target.result;
	};
	reader.readAsDataURL(fileList[0]);
});

/* BUTTON SETTING */
// brush
btn.brush.addEventListener("click", function(){
	type = 0;
	ctx.globalCompositeOperation = "source-over";
	textSetting.classList.add("hidden");
	imageSetting.classList.add("hidden");
});

// circle
btn.circle.addEventListener("click", function(){
	type = 1;
	ctx.globalCompositeOperation = "source-over";
	textSetting.classList.add("hidden");
	imageSetting.classList.add("hidden");
});

// rect
btn.rect.addEventListener("click", function(){
	type = 2;
	ctx.globalCompositeOperation = "source-over";
	textSetting.classList.add("hidden");
	imageSetting.classList.add("hidden");
});

// tri
btn.tri.addEventListener("click", function(){
	type = 3;
	ctx.globalCompositeOperation = "source-over";
	textSetting.classList.add("hidden");
	imageSetting.classList.add("hidden");
});

// line
btn.line.addEventListener("click", function(){
	type = 4;
	ctx.globalCompositeOperation = "source-over";
	textSetting.classList.add("hidden");
	imageSetting.classList.add("hidden");
});

// eraser
btn.eraser.addEventListener("click", function(){
	type = 5;
	ctx.globalCompositeOperation = "destination-out";
	textSetting.classList.add("hidden");
	imageSetting.classList.add("hidden");
});

// text
btn.text_.addEventListener("click", function(){
	type = 6;
	ctx.globalCompositeOperation = "source-over";
	textSetting.classList.remove("hidden");
	imageSetting.classList.add("hidden");
});

// image
btn.img.addEventListener("click", function(){
	type = 7;
	ctx.globalCompositeOperation = "source-over";
	textSetting.classList.add("hidden");
	imageSetting.classList.remove("hidden");
});

// clean canvas
func.rst.addEventListener("click", function(){
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	recordStep();
});

// undo
func.undo.addEventListener("click", function(){
	
	if(stepIndex>0){
		if(type==5)
			ctx.globalCompositeOperation = "source-over";
		var htmlImg = new Image();
		htmlImg.onload = function(){
			ctx.clearRect(0, 0, canvas.width, canvas.height);
			ctx.drawImage(htmlImg, 0, 0, canvas.width, canvas.height);
			if(type==5)
				ctx.globalCompositeOperation = "destination-out";
		};
		stepIndex--;
		htmlImg.src = stepRecord[stepIndex];
	}else if(stepIndex==0){
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		stepIndex--;
	}
	
	// change apperence of un/redo button
	if(stepIndex<0){
		func.undo.style.backgroundImage = 'url("img/button/undo_ban.png")';
	}
	if(stepIndex<maxRedoStep){
		func.redo.style.backgroundImage = 'url("img/button/redo.png")';
	}
	
})

// redo
func.redo.addEventListener("click", function(){
	if(stepIndex<maxRedoStep){
		if(type==5)
			ctx.globalCompositeOperation = "source-over";
		
		var htmlImg = new Image();
		htmlImg.onload = function(){
			ctx.clearRect(0, 0, canvas.width, canvas.height);
			ctx.drawImage(htmlImg, 0, 0, canvas.width, canvas.height);
			if(type==5)
				ctx.globalCompositeOperation = "destination-out";
		};
		htmlImg.src = stepRecord[++stepIndex];
		
		func.undo.style.backgroundImage = 'url("img/button/undo.png")';
	}
	
	// change apperence of un/redo button
	if(stepIndex>=maxRedoStep){
		func.redo.style.backgroundImage = 'url("img/button/redo_ban.png")';
	}
})

// download images
func.download.addEventListener("click", function(){
	download.href = canvas.toDataURL();
});
